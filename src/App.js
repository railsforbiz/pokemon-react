import React, { Component } from 'react';
import Pokedex from './Pokedex.js';
import MyLineup from './MyLineup.js';
import PokemonEquip from './PokemonEquip.js'
import './App.css';



class App extends Component {

// const Button = () => 
//   <button>testing</button>


  render() {
    return (
      <div className='app-master'>
        <h1>Pokemon App Reactjs</h1>
        <div className='row'>
          <div className='col-md-6'>
            <MyLineup />
            <PokemonEquip />
          </div>
          <div className='col-md-6'>
            <Pokedex />
          </div>
        </div>
      </div>
    );
  }
}












export default App;
