import React, { Component } from 'react';




class Pokedex extends Component {
  constructor(){
    super();
    this.state = {pokemons: [],
      url: [],
    sprites: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
    }
    
  }

  componentWillMount(){
      fetch('https://pokeapi.co/api/v2/pokemon/')
      .then( response => response.json())
      .then(({results: pokemons}) => this.setState({pokemons}))
  }

  filter(e){
    this.setState({filter: e.target.value})
  }

  pokechange(){
    // fetch({props.pokelist.url})
  }

  render(){
      let pokemons = this.state.pokemons
      // let pokeurl = this.state.pokemons.name

      if(this.state.filter){
       pokemons = pokemons.filter(pokemon =>
      pokemon.name.toLowerCase()
      .includes(this.state.filter.toLowerCase()))
  }

    return (
    <div className='card pokedex'>
      <h4>Pokedex</h4>
      <input type='text' placeholder='Search' onChange={this.filter.bind(this)}/>
      {pokemons.map(pokemon =>
      <PokeList key={pokemon.name} pokelist={pokemon} /> 
      )}

      
    </div>
    )
  }


}




const PokeList = (props) => 
<div>
<img alt={props.pokelist.name} src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"/>
  <p>{props.pokelist.name}</p>
</div>
// const PokeSprites = (props) =>
//   <img alt='sprites' src={pokemon.sprites}/>



export default Pokedex;